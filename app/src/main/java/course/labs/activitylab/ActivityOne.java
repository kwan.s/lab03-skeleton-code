package course.labs.activitylab;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;

import org.w3c.dom.Text;

public class ActivityOne extends Activity {

    // string for logcat documentation
    private final static String TAG = "Lab-ActivityOne";

    // lifecycle counts
    int createCounter = 0;
    int startCounter = 0;
    int resumeCounter = 0;
    int pauseCounter = 0;
    int stopCounter = 0;
    int restartCounter = 0;
    int destroyCounter = 0;

    // TextViews
    TextView resultCreate, resultStart, resultResume, resultPause, resultStop, resultRestart, resultDestroy;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_one);
        //Log cat print out
        Log.i(TAG, "onCreate called");

        SharedPreferences prefs = getPreferences(MODE_PRIVATE);

        resultCreate = (TextView) findViewById(R.id.createCounter);
        resultStart = (TextView) findViewById(R.id.startCounter);
        resultResume = (TextView) findViewById(R.id.resumeCounter);
        resultPause = (TextView) findViewById(R.id.pauseCounter);
        resultStop = (TextView) findViewById(R.id.stopCounter);
        resultRestart = (TextView) findViewById(R.id.restartCounter);
        resultDestroy = (TextView) findViewById(R.id.destroyCounter);

        resultCreate.setText(prefs.getString("create", null));
        resultStart.setText(prefs.getString("start", null));
        resultResume.setText(prefs.getString("resume", null));
        resultPause.setText(prefs.getString("pause", null));
        resultStop.setText(prefs.getString("stop", null));
        resultRestart.setText(prefs.getString("restart", null));
        resultDestroy.setText(prefs.getString("destroy", null));

        createCounter = Integer.parseInt(prefs.getString("create", null));
        startCounter = Integer.parseInt(prefs.getString("start", null));
        resumeCounter = Integer.parseInt(prefs.getString("resume", null));
        pauseCounter = Integer.parseInt(prefs.getString("pause", null));
        stopCounter = Integer.parseInt(prefs.getString("stop", null));
        restartCounter = Integer.parseInt(prefs.getString("restart", null));
        destroyCounter = Integer.parseInt(prefs.getString("destroy", null));

        //updateText();
        /*
        if ((savedInstanceState != null)) {
            createCounter = savedInstanceState.getInt("create");
            startCounter = savedInstanceState.getInt("start");
            resumeCounter = savedInstanceState.getInt("resume");
            pauseCounter = savedInstanceState.getInt("pause");
            stopCounter = savedInstanceState.getInt("stop");
            restartCounter = savedInstanceState.getInt("restart");
            destroyCounter = savedInstanceState.getInt("destroy");

        }
        */
        // Increment & display
        createCounter++;
        resultCreate.setText(Integer.toString(createCounter));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.activity_one, menu);
        return true;
    }

    // lifecycle callback overrides

    @Override
    public void onStart() {
        super.onStart();
        //Log cat print out
        Log.i(TAG, "onStart called");

        // Increment & display
        startCounter++;
        resultStart.setText(Integer.toString(startCounter));
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.i(TAG, "onResume called");

        // Increment & display
        resumeCounter++;
        resultResume.setText(Integer.toString(resumeCounter));
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.i(TAG, "onPause called");

        String strTemp;

        // Increment & display
        pauseCounter++;
        resultPause.setText(Integer.toString(pauseCounter));

        SharedPreferences prefs = getPreferences(MODE_PRIVATE);
        SharedPreferences.Editor edit = prefs.edit();

        strTemp = resultCreate.getText().toString();
        edit.putString("create", strTemp);
        strTemp = resultStart.getText().toString();
        edit.putString("start", strTemp);
        strTemp = resultResume.getText().toString();
        edit.putString("resume", strTemp);
        strTemp = resultPause.getText().toString();
        edit.putString("pause", strTemp);
        strTemp = resultStop.getText().toString();
        edit.putString("stop", strTemp);
        strTemp = resultRestart.getText().toString();
        edit.putString("restart", strTemp);
        strTemp = resultDestroy.getText().toString();
        edit.putString("destroy", strTemp);

        edit.commit();
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.i(TAG, "onStop called");

        // Increment & display
        stopCounter++;
        resultStop.setText(Integer.toString(stopCounter));
    }

    @Override
    public void onRestart() {
        super.onRestart();
        Log.i(TAG, "onRestart called");

        // Increment & display
        restartCounter++;
        resultRestart.setText(Integer.toString(restartCounter));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i(TAG, "onDestroy called");

        // Increment & display
        destroyCounter++;
        resultDestroy.setText(Integer.toString(destroyCounter));
    }

    // Note:  if you want to use a resource as a string you must do the following
    //  getResources().getString(R.string.stringname)   returns a String.

    /*
    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        savedInstanceState.putInt("create", createCounter);
        savedInstanceState.putInt("start", startCounter);
        savedInstanceState.putInt("resume", resumeCounter);
        savedInstanceState.putInt("pause", pauseCounter);
        savedInstanceState.putInt("stop", stopCounter);
        savedInstanceState.putInt("restart", restartCounter);
        savedInstanceState.putInt("destroy", destroyCounter);
    }

    @Override
    protected void onRestoreInstanceState(Bundle restoreInstanceState) {
        super.onRestoreInstanceState(restoreInstanceState);
        createCounter = restoreInstanceState.getInt("create");
        startCounter = restoreInstanceState.getInt("start");
        resumeCounter = restoreInstanceState.getInt("resume");
        pauseCounter = restoreInstanceState.getInt("pause");
        stopCounter = restoreInstanceState.getInt("stop");
        restartCounter = restoreInstanceState.getInt("restart");
        destroyCounter = restoreInstanceState.getInt("destroy");
    }
    */

    public void updateText() {
        resultCreate.setText(Integer.toString(createCounter));
        resultStart.setText(Integer.toString(startCounter));
        resultResume.setText(Integer.toString(resumeCounter));
        resultPause.setText(Integer.toString(pauseCounter));
        resultStop.setText(Integer.toString(stopCounter));
        resultRestart.setText(Integer.toString(restartCounter));
        resultDestroy.setText(Integer.toString(destroyCounter));
    }

    public void clear(View v) {
        createCounter = 0;
        startCounter = 0;
        resumeCounter = 0;
        pauseCounter = 0;
        stopCounter = 0;
        restartCounter = 0;
        destroyCounter = 0;
        resultCreate.setText(Integer.toString(createCounter));
        resultStart.setText(Integer.toString(startCounter));
        resultResume.setText(Integer.toString(resumeCounter));
        resultPause.setText(Integer.toString(pauseCounter));
        resultStop.setText(Integer.toString(stopCounter));
        resultRestart.setText(Integer.toString(restartCounter));
        resultDestroy.setText(Integer.toString(destroyCounter));
    }

    public void launchActivityTwo(View view) {
        startActivity(new Intent(this, ActivityTwo.class));
    }
}
